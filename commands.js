var http = require('http');
var fs = require('fs');

function RetrieveJSON(url, callback)
{
    var https = require('https');
    https.get(url, function (response)
    {
        var data = '';

        response.on('data', (chunk) => {
            data += chunk;
        });

        response.on('end', () => 
        {
            var obj = JSON.parse(data);
            
            callback && callback(obj);
        });
    });
}

exports.admins = 
[
    "124136556578603009", 
    "398649381932236810" //You
]

exports.cmds = 
[
    {
        cmd: "help",
        params: "none",
        execute: function(bot, info, args)
        {
            var cmds = [];
            
            for(var i = 0; i < bot.commands.length; i++)
            {
                if(!bot.commands[i].hidden)
                {
                    var lines = "";

                    for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                        lines += "-";

                    cmds.push ("$" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                }
            }

            bot.sendMessage({
                to: info.channelID,
                message: "```" + cmds.join("\n") + "```",
                typing: false
            });
        }
    },
    {
        cmd: "debughelp",
        params: "none",
        hidden: true,
        execute: function(bot, info, args)
        {
            var cmds = ["Commands:"];
            var hidden = ["Admin-only commands:"];
            
            for(var i = 0; i < bot.commands.length; i++)
            {
                var lines = "";

                for(var a = 0; a < 15 - bot.commands[i].cmd.length; a++)
                    lines += "-";

                if(!bot.commands[i].hidden)
                    cmds.push ("$" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
                else
                    hidden.push ("$" + bot.commands[i].cmd + " " + lines + " parameters: " + bot.commands[i].params);
            }

            bot.sendMessage({
                to: info.channelID,
                message: "Admins: <@" + exports.admins.join("> <@") + "> ```" + cmds.join("\n") + "\n\n" + hidden.join("\n") + "```",
                typing: false
            });
        }
    },
    {
        cmd: "invitelink",
        params: "none",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "https://discordapp.com/oauth2/authorize?&client_id=480772699996553257&scope=bot&permissions=511040",
                typing: false
            });
        }
    },
    {
        cmd: "ping",
        params: "none",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "<@!" + info.userID + ">" + ' Pong!',
                typing: true
            });
        }
    }, 
    {
        cmd:"reload",
        params: "none",
        hidden:true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "Updating and reloading!",
                typing: false
            }, function()
            {
                bot.suicide();
            });
        }
    },
    {
        cmd: "pat",
        params: "@mention (optional, omit to pat me!)",
        execute:function(bot, info, args)
        {
            if(args.length == 0)
            {
                bot.addReaction({
                    channelID: info.channelID,
                    messageID: info.evt.d.id,
                    reaction: "❤"
                });

                bot.sendMessage({
                    to: info.channelID,
                    message: ":3",
                    typing: true
                });

                bot.data.pats = bot.data.pats || {};

                bot.data.pats[info.userID] = (bot.data.pats[info.userID] || 0) - -1;

                bot.saveData();
            }
            else
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "_pats " + args[0] + "_",
                    typing: true
                }); 
            }
        }
    },
    {
        cmd: "favorite",
        params: "none",
        execute: function(bot, info, args)
        {
            patters = bot.data.pats || {};

            var fav = 0;
            var pats = 0;

            console.log(patters);

            for(var key in patters)
            {
                 if(patters[key] > pats)
                 {
                     fav = key;
                     pats = patters[key];
                 }
            };

            if(fav !== 0)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "❤<@!" + fav + ">❤",
                    typing: true
                });
            }
            else
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "idk",
                    typing: true
                });
            }
        }
    },
    {
        cmd: "echo",
        params: "anything",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: args.join(" "),
                typing: true
            });
        }
    },
    {
        cmd: "memdump",
        params: "anything",
        hidden: true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + JSON.stringify(bot.data) + "```",
                typing: false
            });
        }
    },
    {
        cmd: "globalmemdump",
        params: "anything",
        hidden: true,
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "```" + JSON.stringify(bot.servers) + "```",
                typing: false
            });
        }
    },
    {
        cmd: "google",
        params: "search query",
        execute:function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: "http://lmgtfy.com/?q=" + args.join("+"),
                typing:true
            });
        }
    },
    {
         cmd: "punch",
         params: "Self explanatory",
         execute:function(bot, info, args)
         {
             bot.sendMessage({
                 to: info.channelID,
                 message: "FALCON PUNCH" + "!",
                 typing:true
             });
         }
    },
    {
         cmd: "random",
         params: "Nothing at all!",
         execute:function(bot, info, args)
         {
             bot.sendMessage({
                 to: info.channelID,
                 message: "Rolled a " + Math.round(Math.random() * 100) + "!",
                 typing:true
             })
         }
    },
    {
        cmd: "imfunny",
        params: "You're funny",
        execute:function(bot, info, args)
        {

            bot.sendMessage({
                to: info.channelID,
                message: "Suuuuuuure, " + "<@!" + info.userID + ">" + ". Sure.",
                embed: {
                    title: "Sure",
                    url: "https://giphy.com/",
                    image: {
                    url: "https://media1.giphy.com/media/fZVYBAM9wtRrLd3q6O/giphy.gif",
                    }
                },
                typing:true
            });
        }
    },
    {
        cmd: "restartpc",
        params: "Please don't use this",
        hidden: true,
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec("shutdown.exe -r -f -t 0", (err, stdout, stderr) => console.log(stdout))
        }
    },
    {
        cmd: "exec",
        params: "Please don't use this",
        hidden: true,
        execute: function(bot, info, args)
        {
            var command = args.join(" ");

            var exec = require('child_process').exec
            exec(command, (err, stdout, stderr) => 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "```" + stdout + "```",
                    typing:false
                });
            });
        }
    }, 
    {
        cmd: "pokemon",
        params: "pokemon id",
        execute: function(bot, info, args)
        {
            RetrieveJSON("https://pokeapi.co/api/v2/pokemon/" + args[0] + "/", function(data)
            {
                var returnStr = data.name;

                bot.sendMessage({
                    to: info.channelID,
                    message: "Found '" + returnStr + "'",
                    typing: true
                });
            });
        }
    },
    {
        cmd: "version",
        params: "none",
        execute: function(bot, info, args)
        {
            var exec = require('child_process').exec
            exec('git log --oneline -n 1 HEAD', (err, stdout, stderr) => 
            {
                console.log(stdout)
                bot.sendMessage({
                    to: info.channelID,
                    message: "Current version: ```" + stdout + "```",
                    typing:false
                });
            });
        }
    },
    {
        cmd: "template",
        params: "It's a template",
        execute: function(bot, info, args)
        {
        bot.sendMessage({
            to: info.channelID,
            message: "Seriously, it's just a template.",
            typing:true
           })
        }
    },
    {
        cmd: "addnote",
        params: "Store notes",
        execute: function(bot, info, args)
        {
            var addNote = 
            {
                title: "newTitle",
                content: "newContent"
            }
            bot.data.notes[info.userID][newNote.title] = newNote,

            bot.sendMessage({
                to: info.channelID,
                message: "Note added!",
                typing: true
            })

            args.join(" ")
        }
    },
    {
        cmd: "praise",
        params: "Praise the sun",
        execute: function(bot, info, args)
        {
            bot.sendMessage({
                to: info.channelID,
                message: 'JOLLY COOPERATION "\\"[T]/ PRAISE THE SUN',
                typing: true
            })
        }
    },
]
