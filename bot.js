var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');

var cmdsFile = require('./commands.js');
var commands = cmdsFile.cmds;
var admins = cmdsFile.admins;
console.log(commands);

var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});

bot.commands = commands;

bot.servers = {};

bot.saveData = function(callback, context)
{
    bot.servers[bot.data.serverId] = bot.data || {};

    fs.writeFile("botData.json", JSON.stringify(bot.servers), function(err) {
        if (err) {
            console.log(err);
        }

        context = context || this;
        callback && callback.call(context);
    });
}

bot.on('ready', function (evt) {
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');

    fs.readFile('./botData.json', function read(err, data) {
        if (err) {
            throw err;
        }
        bot.servers = JSON.parse(data);
    });
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `$`
    if (message.substring(0, 1) == '$') {                                           // << Bot command prefix here
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        var info = {user:user, userID:userID, channelID:channelID, message:message, evt:evt};

        var id = evt.d.guild_id || userID;

        bot.data = bot.servers[id] || {};

        bot.data.serverId = id;

        args = args.splice(1);
        for(var i = 0; i < commands.length; i++)
        {
            if(cmd.toLowerCase() == commands[i].cmd)
            {
                if(commands[i].hidden && admins.indexOf(info.userID) == -1)
                    return;

                commands[i].execute(bot, info, args);
                break;
            }
        }
     }
});